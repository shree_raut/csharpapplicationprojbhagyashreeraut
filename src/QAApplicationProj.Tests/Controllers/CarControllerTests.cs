﻿using QAApplicationProj.Controllers;
using Xunit;
using QAApplicationProj.Tests.Data;
using QAApplicationProj.Requests;
using QAApplicationProj.Responses;
using System;

namespace QAApplicationProj.Tests.Controllers
{
    public class CarControllerTests
    {
        private readonly CarController _controller;
        public CarControllerTests()
        {
            /* TODO; Hint: figure out a solution to avoid communicating with the database, then change the null reference to an instance of ICarRepository. 
             * You can choose to use a 3rd party library in NuGet if you'd like. */
            _controller = new CarController(new MockCarRepository());
        }


        [Fact]
        public void CreateCar_HappyCase()
        {
            CarRequest request = new CarRequest { Make = "Hundai", Model = "Sonata", Year = 2015 };
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.True(response.WasSuccessful);
        }

        [Fact]
        public void CreateCar_ReturnsResponseInvalidCar_ForEmptyMake()
        {
            CarRequest request = new CarRequest { Make = "", Model = "Sonata", Year = 2015 };
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.Equal("The request did not pass validation.", response.Message);
        }

        [Fact]
        public void CreateCar_ReturnsResponseInvalidCar_ForEmptyModel()
        {
            CarRequest request = new CarRequest { Make = "Hundai", Model = "", Year = 2015 };
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.Equal("The request did not pass validation.", response.Message);
        }

        [Fact]
        public void CreateCar_ReturnsResponseValid_ForSpecialCharacter()
        {
            CarRequest request = new CarRequest { Make = "Hun$#@dai", Model = "abc", Year = 2015 };
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.True(response.WasSuccessful);
        }


        [Fact]
        public void CreateCar_ReturnsResponseInvalidCar_ForInvalidYear()
        {
            CarRequest request = new CarRequest { Make = "Hundai", Model = "Sonata", Year = 0 };
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.Equal("The request did not pass validation.", response.Message);
        }

        [Fact]
        public void CreateCar_ReturnsResponseCarExists_ForDuplicateCar()
        {
            CarRequest request = new CarRequest { Make = "Honda", Model = "Accord", Year = 2015 };
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.Equal("The requested Car already exists.", response.Message);
        }

        /*
        // TO DO 
        Check if initial set up allows to simulate unhandlled exceptions

        [Fact]
        public void CreateCar_ReturnsResponseUnhandledExceptionOccured_ForUnhandledException()
        {
            CarRequest request = new CarRequest { Make = "", Model = "" , Year =  };
            
            CreateCarResponse response = _controller.CreateCar(request);
            Assert.Equal("An unhandled exception occurred while creating the requested Car.", response.Message);
        }
        */

    }
}
