﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using QAApplicationProj.Data.Entities;
using QAApplicationProj.Data.Repositories.Interfaces;

namespace QAApplicationProj.Data.Repositories
{
    public class CarRepository : ICarRepository {
        private static readonly ICollection<Car> Cars = new List<Car> {
            new Car() {
                Make = "Honda",
                Model = "Civic",
                Year = 2010
            },
            new Car() {
                Make = "Ferrari",
                Model = "Testarossa",
                Year = 1989
            },
            new Car() {
                Make = "Lamborghini",
                Model = "Murcielago",
                Year = 2006
            },
        };

        /// <summary>
        /// Simulates a database query to figure out if a Car exists or not by using an internal collection of Cars.
        /// Note that the Make, Model, and Year are used for comparison in the Car's Equals() method.
        /// </summary>
        /// <param name="car"></param>
        /// <returns>true if the car exists</returns>
        public bool Exists(Car car) {
            if (car == null) {
                throw new ArgumentNullException(nameof(car));
            }

            Task.Delay(2000).Wait();
            return Cars.Contains(car);
        }

        /// <summary>
        /// Simulates a database query to Create a car by using an internal collection of Cars. Throws an exception if the Car already exists.
        /// </summary>
        /// <param name="car"></param>
        /// <returns>true if the car exists</returns>
        public void Create(Car car) {
            if (car == null) {
                throw new ArgumentNullException(nameof(car));
            }

            if (Exists(car)) {
                throw new InvalidOperationException("Cannot create a Car that already exists.");
            }

            Task.Delay(2000).Wait();
            Cars.Add(car);
        }
    }
}
