﻿using Xunit;
using QAApplicationProj.Helpers;

namespace QAApplicationProj.Tests.Helpers
{
    public class FareHelperTests
    {
        [Theory]
        [InlineData(10.5, 14.2)]
        public void AddFares_HappyCase(decimal fare1, decimal fare2)
        {
            Assert.Equal(24.7m, FareHelper.AddFares(fare1, fare2), 2);
        }
        [Theory]
        [InlineData(0, 0)]
        public void AddFares_ReturnsZero_WhenBothFaresZero(decimal fare1, decimal fare2)
        {
            Assert.Equal(0, FareHelper.AddFares(fare1, fare2), 2);
        }

        [Theory]
        [InlineData(-10.5, 14.2)]
        public void AddFares_ReturnsException_FirstFareNegative(decimal fare1, decimal fare2)
        {
            System.Exception ex = Assert.Throws<System.ArgumentOutOfRangeException>(() => FareHelper.AddFares(fare1, fare2));
            Assert.Contains("The first fare must be positive.", ex.Message);
        }
        [Theory]
        [InlineData(10.5,-14.2)]
        public void AddFares_ReturnsException_SecondFareNegative(decimal fare1, decimal fare2)
        {
            System.Exception ex = Assert.Throws<System.ArgumentOutOfRangeException>(() => FareHelper.AddFares(fare1, fare2));
            Assert.Contains("The second fare must be positive.", ex.Message);
        }
        [Theory]
        [InlineData(-10.5, -14.2)]
        public void AddFares_ReturnsException_BothFaresNegative(decimal fare1, decimal fare2)
        {
            System.Exception ex = Assert.Throws<System.ArgumentOutOfRangeException>(() => FareHelper.AddFares(fare1, fare2));
            Assert.Contains("The first fare must be positive.", ex.Message);
        }
    }
}
