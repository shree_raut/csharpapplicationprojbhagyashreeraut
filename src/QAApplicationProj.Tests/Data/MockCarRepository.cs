﻿

using System;
using QAApplicationProj.Data.Entities;
using QAApplicationProj.Data.Repositories.Interfaces;
using System.Collections.Generic;

namespace QAApplicationProj.Tests.Data
{
    public class MockCarRepository : ICarRepository
    {
        List<Car> carList = new List<Car>
        {
            new Car() {Make = "Honda" , Model = "Accord", Year = 2015 },
            new Car() {Make = "Honda" , Model = "Civic", Year = 2014 },
            new Car() {Make = "Toyota" , Model = "Corolla", Year = 2016 },
            new Car() {Make = "Toyota" , Model = "Camry", Year = 2015 },
            new Car() {Make = "BMW" , Model = "328i", Year = 2014 },
            new Car() {Make = "Audi" , Model = "A4", Year = 2016 },
            new Car() {Make = "Chevrolet" , Model = "Camaro", Year = 2015 }
        };

        public void Create(Car car)
        {
            if (car == null)
            {
                throw new ArgumentNullException(nameof(car));
            }

            if (Exists(car))
            {
                throw new InvalidOperationException("Cannot create a Car that already exists.");
            }

            carList.Add(car);
        }

        public bool Exists(Car car)
        {
            if (car == null)
            {
                throw new ArgumentNullException(nameof(car));
            }

            return carList.Contains(car);
        }
    }
}
